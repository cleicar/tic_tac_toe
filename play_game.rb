require 'optparse'

require_relative 'lib/tic_tac_toe.rb'

options = {}

OptionParser.new do |opts|
  opts.on('-n [ARG]', '--name [ARG]', "Specify the player's name") do |value|
    options[:name] = value
  end
end.parse!


new_game = TicTacToe::Game.new(options[:name])
new_game.start_game
