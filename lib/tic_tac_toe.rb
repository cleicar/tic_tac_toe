require_relative "board"
require_relative "player"

module TicTacToe
	class Game
		attr_accessor :positions, :computer, :game, :board, :player_1, :player_2, :winner

		def initialize(player_name)
			@player_1 = Player.new(player_name, 'X', Player::HUMAN)
			@player_2 = Player.new('Computer',  'O', Player::COMPUTER)

			@board = Board.new(player_name)
		end

		def start_game
			until game_is_over or board.is_tied?
				board.display
				get_human_spot
				get_computer_spot
			end

			board.display_result(get_winner)
		end

		private
		def game_is_over
			game_over = false

			Board::WINNER_POSITIONS.each do |winner_positions|
				values = winner_positions.map{ |index| board.positions[index]}
				if values.uniq.length == 1
					game_over = true
					break
				end
			end

			return game_over
		end

		def get_human_spot
			valid_move = false

			until valid_move
				choosen_position = STDIN.gets.chomp

				if is_valib_number? choosen_position
					if board.available_positions.include?(choosen_position.to_i - 1)
						board.positions[choosen_position.to_i - 1] = player_1.symbol
						valid_move = true
						set_winner(player_1) if game_is_over
					else
						STDERR.print "This position is not available! Please choose another one: "
					end
				else
					STDERR.print "Only numbers are allowed. Please choose (1 to 9): "
				end

			end
		end

		def get_computer_spot
			best_position = get_best_move
			board.positions[best_position] = player_2.symbol if best_position
			set_winner(player_2) if game_is_over
		end

		def get_best_move
			best_move = nil

			if board.positions[4] == "5"
				best_move = 4
			else
				available_positions = board.available_positions

				available_positions.each do |available|
					if player_will_win(player_2.symbol, available) or player_will_win(player_1.symbol, available)
						best_move = available
						break
					else
						best_move = rand(0..available_positions.count)
					end
				end
			end

			return best_move
		end

		def player_will_win(symbol, position)
			aux = board.positions[position].dup

			board.positions[position] = symbol

			game_over = game_is_over

			board.positions[position] = aux

			return game_over
		end

		def set_winner(player)
			player.is_winner = true
		end

		def get_winner
			board.is_tied? ? 'tied' : players.find { |player| player.is_winner }
		end

		def players
			[player_1, player_2]
		end

		def players_symbols
			[player_1.symbol, player_2.symbol]
		end

		def is_valib_number?(value)
			Float(value) != nil rescue false
		end
	end
end
