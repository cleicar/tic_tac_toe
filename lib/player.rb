class Player
	attr_accessor :name, :symbol, :type, :is_winner

	COMPUTER  = 0
	HUMAN 		= 1

	def initialize(name, symbol, type)
		@name 	  	= name
		@symbol   	= symbol
		@type				= type
		@is_winner 	= false
	end
end
