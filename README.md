# Tic-tac-toe

Tic-Tac-Toe is a game to play on terminal. At the game, two players (X and O) take turns marking the board spaces in a 3x3 grid.

At this implementation, you will play against the machine. You will be the X symbol, and machine will be the O.

## Usage

1. Start the game.

	```sh
	$ ruby play_game.rb --name "Your Name"
	```

2. Fill a position on the board choosing a number (1-9)

3. The machine will choose another number

4. Is the player turn again and so on.

5. The player who succeeds in placing three of his symbol in a horizontal, vertical, or diagonal row wins the game.

6. A tie happens when there is no more positions to fill and nobody has won.

## Attention

* Only valid numbers are allowed when choosing a position
* Do not choose a position who was already taken
