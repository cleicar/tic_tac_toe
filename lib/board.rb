require_relative "player"

class Board
  attr_reader :grid, :positions, :player_name

  WINNER_POSITIONS = [
    [0,1,2],[3,4,5],[6,7,8],  # vertical
    [0,3,6],[1,4,7],[2,5,8],  # horizontal
    [0,4,8],[2,4,6]           # diagonal
  ]

  def initialize(player_name)
    @positions   = %w[1 2 3 4 5 6 7 8 9]
    @player_name = player_name.upcase
  end

  def display
    header
    puts "\t+---+---+---+"
    puts "\t| #{positions[0]} | #{positions[1]} | #{positions[2]} |"
    puts "\t+---+---+---+"
    puts "\t| #{positions[3]} | #{positions[4]} | #{positions[5]} |"
    puts "\t+---+---+---+"
    puts "\t| #{positions[6]} | #{positions[7]} | #{positions[8]} |"
    puts "\t+---+---+---+"
    print "Your turn! Please, choose a position (1 to 9): "
  end

  def display_result(winner)
    self.display

    case winner
    when 'tied'
      puts "\n=========================================="
      puts " Sorry! We tied! "
      puts "=========================================="
    when winner.type == Player::HUMAN
      puts "\n=========================================="
      puts " CONGRATULATIONS, #{winner.name}! You won!"
      puts "=========================================="
    else
      puts "\n=========================================="
      puts " Sorry, I won!"
      puts "=========================================="
    end
  end

  def available_positions
    positions.map.with_index { |position, index| ['X', 'O'].include?(position) ? nil : index }.compact
  end

  def is_tied?
    available_positions.size == 0
  end

  private
  def header
    Gem.win_platform? ? (system "cls") : (system "clear")
    puts "========================================"
    puts " Hello, #{player_name}. Welcome! "
    puts "========================================"
    puts " Your symbol is  X                      "
    puts "----------------------------------------"
  end
end
